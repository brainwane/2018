---
abstract: In this talk we'll explore designs that go beyond a basic templated @property.
  We'll talk about using the descriptor protocol to encapsulate behavior, create persistent
  and immutable data structures and finally multi-level object hierarchies. The goal?
  To expand your pattern library tool set.
duration: 25
level: Intermediate
room: PennTop South
slot: 2018-10-05 14:00:00-04:00
speakers:
- Owein Reese
title: Pushing the Bounds with Descriptors
type: talk
---

The problem with most online tutorials is that they lack depth, often involving trivially simple examples. In the case of descriptors, the issue is compounded by the complexity of interface exposed by the protocol itself. Here, simplified examples are chosen so that other issues can be discussed such as the avoidance or management of global state. This deprives the audience of a deep exploration of the rich dynamics descriptors lend to the object oriented nature of Python.

In this talk, we'll go beyond basic @property templates. Building along a central theme, a solution will evolve to handle more demanding needs. As the approach changes, we'll discuss encapsulating behavior, immutable and partially persistent data structures, and then delve into the interplay between multi-level, dependent object hierarchies.

The goal of all this? To expand the realm of possibilities in your own pattern library and enrich the tool set you bring with you to new problems.