---
abstract: The first step to creating a diverse workforce often to collect demographics
  data on your employees, but even this is challenging. In this talk, we discuss how
  we think about these challenges, strategies we implemented, and suggestions so others
  may have an easier time collecting this data.
duration: 30
level: All
room: PennTop South
slot: 2018-10-05 13:30:00-04:00
speakers:
- Nicole Zuckerman
title: Best Intentions, Least Measured
type: talk
---

The fact that tech is struggling to hire or retain employees from diverse backgrounds has been written about and discussed thoroughly, particularly in the last few years.  The economic, societal, and moral benefits of diversity are also well documented.  Why is it hard, then, for well-intentioned organizations to shift their demographics?  There are a number of reasons, but one that doesn’t appear to have been thoroughly discussed already is the challenge of gathering and responding to data about diversity within a company’s hiring pool and existing employees.

Many organizations have at least made some efforts to increase diversity, inclusion, or both; but how do you know which strategies were successful? What does success mean, and how do you measure the results of your effort?  Where do you even start?

Data driven decision making is very popular in tech, but in order to measure an increase, you first need to know your starting point, and then you need to set up a repeatable data collection process to measure the change over time.

We will describe the barriers to collecting information from employees and the candidate pool that prevent organizations from knowing what’s working, pitfalls to watch out for, and some tools that have helped us obtain data necessary to make informed choices and improve our diversity and inclusion.