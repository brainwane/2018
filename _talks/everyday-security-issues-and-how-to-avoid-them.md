---
abstract: "Security is hard, yet vital for any software these days. After
  some motivational facts, this talk will take you on a brief tour in secure
  software design and illustrate common security issues. Topics include threat
  analysis, deployment, parsing, authentication, TLS/SSL, crypto, and more."
duration: 30
level: Intermediate
room: PennTop South
slot: 2018-10-05 10:45:00-04:00
speakers:
- Christian Heimes
title: 'Everyday security issues and how to avoid them'
type: talk
---

These days virtually all software and computer hardware is connected to the
internet. Ultimately the internet is a hostile place and filled with people
that will attempt to abuse any vulnerability for fun, profit or more
sinister reasons. Therefore every software developer and administrator
should have at least a basic understanding how to develop and run code
securely. After all you don’t want to become the laughing stock on hacker
news or cause your company to loose billions in shareholder value.

This talk won’t turn you into a security specialist over night, but you will
learn how to avoid common mistakes in your daily work. I will introduce you
to best practices and prevalent security bugs, hilarious anecdotes and some
real life examples from my daily work as security engineer. The presentation
features airplanes, squirrels, ducks, and the most efficient password
cracking method: not as brutucal as XKCD 538 and much more delicious.
