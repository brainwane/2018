---
abstract: "Have you written software? Have you had mistakes or bugs in your code?
  I suspect the answer to both is yes. \nThe topic of famous bugs in history has fascinated
  me for a long time, in this talk I will discuss some of the most famous ones and
  how we can learn from them."
duration: 30
level: All
room: PennTop South
slot: 2018-10-06 10:45:00-04:00
speakers:
- Ian Zelikman
title: The stories of the most infamous bugs
type: talk
---

Whenever we write code we eventually make mistakes and have “bugs”. We employ many techniques including testing and reviews to avoid them but some mistakes still make it into production.

The topic of famous bugs in history has intrigued me for a very long time and it is a subject I enjoy researching. 
In this talk we will explore 5 of the most famous bugs, their consequences and what we can learn from them:

* Why the Mars climate orbiter did not orbit 
* How the Ariane-5 rocket was lost 
* Waiting for the long distance AT&T call 
* How the world didn’t end in September 1983 
* The fatal Patriot missile miscalculation