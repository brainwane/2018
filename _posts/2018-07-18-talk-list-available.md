---
title: Talk list now available!
date: 2018-07-18 10:00:00 -0400
call_to_action: /talks/
excerpt_separator: <!--more-->
---

The PyGotham 2018 [talk list](/talks/) is now available. It features three
tracks spread across October 5th and 6th.

<!--more-->

This year's program features over 60 speakers giving 60 talks covering a wide
array of topics, from application design to data science to natural language
processing to web development. There will also be three keynote talks and
lightning talks.

We're really excited about this year's talks and hope to have a schedule ready
in the next few weeks.

Thanks to everyone who submitted a talk, voted on talks, or signed up for the
program committee.
