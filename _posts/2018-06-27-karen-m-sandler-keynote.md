---
title: "Announcing our first keynote: Karen M. Sandler"
date: 2018-06-27 10:00:00 -0400
image: /uploads/posts/karen-m-sandler.jpg
excerpt_separator: <!--more-->
---

Karen M. Sandler is the executive director of the Software Freedom Conservancy.
Karen is known as a cyborg lawyer for her advocacy for free software,
particularly in relation to the software on medical devices. <!--more-->Prior to
joining Conservancy, she was executive director of the GNOME Foundation. Before
that, she was general counsel of the Software Freedom Law Center. Karen
co-organizes Outreachy, the award-winning outreach program for women globally
and for people of color who are underrepresented in US tech. Karen received the
FSF's Award for the Advancement of Free Software and is a recipient of the
O’Reilly Open Source Award.
