---
name: Scott Irwin
talks:
- Dataclasses are here. Now what?
---

Scott Irwin is a senior engineer at Bloomberg LP where he develops Python applications in the Discoverability/Search group. He is also taught Python and other technical topics to Bloomberg engineers as part of the company's internal education programs.
