---
name: Lacey Williams Henschel
talks:
- 'What if Jane Austen had been an engineer? '
---

Lacey Williams Henschel is a developer with REVSYS and part of the organizing team for DjangoCon US. In the past, she's chaired DjangoCon US, organized several Django Girls workshops, taught courses for Treehouse, and written about accessibility at tech events. Previously, she's spoken at DjangoCon Europe, DjangoCon US, Postgres Open, ACT-W Portland, PyDX, and Django Birthday.