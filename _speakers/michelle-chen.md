---
name: Michelle Chen
talks:
- Diversity and Inclusion for all!
---

Michelle Chen is a Senior Software Engineer at Kyruus in Boston. As the Team Lead for Search, she works directly to provide search, scheduling, and data management solutions that help health systems match patients with the right providers. Michelle earned her B.S. and M.Eng in Computer Science from MIT.