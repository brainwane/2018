---
name: David Anderson
talks:
- Choose Your Own Adventure for Client Web Services with GraphQL
---

Dave has been involved with software engineering and development for over 10 years. He is a polyglot passionate about Python 3 and good code in any language. He currently works as an agile developer consultant in New York City and co-hosts the weekly Rabbit Hole podcast, discussing aspects of the human side of development, ranging from TDD and Pair Programming to Functional Programming and DevOps.

LinkedIn: https://www.linkedin.com/in/dvndrsn

Podcast: https://itunes.apple.com/us/podcast/the-rabbit-hole-an-inside-look-into-software-development/id1223811385?mt=2