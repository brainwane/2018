---
name: Kelly Shen
talks:
- "How Etsy Handles \u201CPeeking\u201D in A/B Testing"
---

Kelly Shen is a data engineer at Etsy, where she works on improving the e-commerce company's in-house A/B testing platform. Previously, she was an undergraduate student at MIT studying computer science and mathematics. In her free time, Kelly enjoys reading, painting and listening to music.