---
name: Pearson / InformIT
tier: media
site_url: https://informit.com/python
logo: pearson.jpg
---
InformIT (a part of Pearson, the world’s largest learning company) is your
one-stop technology learning resource. Our passion is delivering trusted and
quality content and resources from the authors, creators, innovators, and
leaders of technology and business from the best publishers in IT including
Pearson Addison-Wesley.
