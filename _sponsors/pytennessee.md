---
name: PyTennessee
tier: community
site_url: https://www.pytennessee.org/
logo: pytn.png
---
PyTennessee is a regional Python conference held every February in Nashville,
TN. PyTennessee 2019 will be our 6th year, and like every year before it,
promises to be our best yet! There's a bit of something for everyone at
PyTennessee 2019, and if you'd like to know more about it, please visit our
[About page](https://www.pytennessee.org/about).
