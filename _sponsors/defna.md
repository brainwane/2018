---
name: DEFNA
tier: financial aid
site_url: https://www.defna.org/
logo: defna.png
---
Django Events Foundation North America (DEFNA) is a non-profit based in
California USA. It was formed in 2015 at the request of the Django Software
Foundation (DSF) to run DjangoCon US. The DSF have licensed DEFNA to run
DjangoCon US for 2015-2020. Beyond DjangoCon US we also plan to be involved with
other events in North America that cover the education and outreach of Django.
