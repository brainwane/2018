---
name: Hearst
tier: silver
site_url: https://hearst.referrals.selectminds.com/magazines-us
logo: hearst.png
---
Hearst Magazines Digital Media reaches more than 100 million people around the world every month on
digital platforms connected to loved and trusted brands like Cosmopolitan, Popular Mechanics,
Esquire, Elle, Runner’s World, and 29 television stations - all served on a microservices based
media publish and distribution platform.
